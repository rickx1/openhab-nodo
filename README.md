# Introduction #

This page describes the Nodo binding for OpenHAB. Nodo is an open source Dutch domotica project based on the Arduino platform. The base unit consists of an Arduino Mega board, an Ethernet shield and a 433MHz transmitter and receiver. There are also slave units with a lower hardware footprint. The system has a plug-in architecture, enabling the control of different devices in your home. Examples are  plugins for 433MHz devices, temperature measurement, smart meter reading, weather stations and smoke detectors. The different analog and digital ports of the Arduino can also be used. More information about the project can be found at [http://www.nodo-domotica.nl](http://www.nodo-domotica.nl).

This binding provides a bridge between the Nodo device and the OpenHAB system. It uses the HTTP interface of the system, also used by the native Nodo web application. This means that there is no physical connection between the Nodo device and the OpenHAB server, all the communication is done over your local network. 

*This binding is still in its early stages but has enough features to be useful. Feel free to experiment and make comments.*

# Binding Configuration #

The Nodo binding uses two way communication using TCP/IP over your local network. In this setup it is essential that both your OpenHAB server and your Nodo device have a fixed IP-address.  First you have to configure your Nodo device (using the serial or telnet connection) and the Nodo binding. To configure the binding you have to add Nodo specific entries to the `openhab.cfg` file:

    ################################### Nodo Binding ####################################
    #
    # The port the nodo connecter listens on
    nodo:server.port=8787
    # The port the nodo device listens on (use server.port for loopback testing without any actual Nodo device) 
    nodo:nodo.port=6636
    # The ip adress of the nodo device (use 127.0.0.1 for loopback testing without any actual Nodo device)
    nodo:nodo.ip=192.168.2.13
    # The password the nodo uses
    nodo:nodo.password=TheSecrectPassword

The `server.port` setting is the port the Nodo binding accepts events from the Nodo device. Make sure that you configure your firewall on your OpenHAB server to accept traffic on this port. You also have to set this port on your Nodo device with the following command:

    PortOutput 8787

*(A Dutch reference of all the Nodo commands can be found [here](http://www.nodo-domotica.nl/index.php/Categorie\:Commando%27s) )*

The `nodo.port` setting is the port the Nodo device accepts commands from the Nodo binding. You can set this with the following Nodo command:

    PortInput 6636

The `nodo.ip` setting is the IP-address of the Nodo device. You can fetch this address with the following Nodo command:

    Status IP

The `nodo.password` settings is the password you configured for your Nodo device. You can change the password on your Nodo device with the following command:

    Password TheSecrectPassword

Next the Nodo device has to know where the Nodo binding can be found. You can do this with the following Nodo command, where you have to supply
 the IP-address of your OpenHAB server:

    HTTPHost 192.168.2.36

Next you have to turn on HTTP output on your Nodo, if you have not already done this. This can be done with the following Nodo command:

    HTTP On

Save the changes you just made with the following Nodo command:

    SettingsSave

Next you first have to start your OpenHAB server and when the server startup is finished restart your Nodo device. A tip is to start the OpenHAB server the first time in debug mode with `start_debug`, this way you see a lot of information in the console. When your communication works well you will see in the console that the Nodo device periodically sends the authentication cookie:

     16:26:10.263 [DEBUG] [.o.b.nodo.internal.NodoBinding:86   ] - Parameters: {cookie=685429C1, id=DFXYHLS1,NanoHttpd.QUER_STRING=id=DFMHHLS1&cookie=685429B1})
     16:26:10.263 [DEBUG] [.o.b.nodo.internal.NodoBinding:57   ] - Hash key: e34149f49a5379258a703d200288ae93 

The event list of the Nodo device controls the behaviour of the device. To get full advantage of your Nodo device you have to use an event list which outputs all events using HTTP output. Make sure that your event list contains at least the following entries:

    WildCard RF,All,0; EventSend HTTP
    WildCard IR,All,0; EventSend HTTP
    WildCard Wired,All,0; EventSend HTTP
    WildCard All,UserEvent,0; EventSend HTTP
    WildCard System,Variable,0; EventSend HTTP

You can use the `EventListShow` Nodo command to display your event list and the `EventListWrite` command to change your event list.

# Item Binding Configuration #

In order to bind an item to Nodo device, you need to provide configuration settings. The easiest way to do so is to add some binding information in your item file (in the folder configurations/items).
### KaKu binding ###

With this binding it is possible to bind a switch to a *Klik-Aan-Klik-Uit* device using the old encoding scheme. The syntax of the binding configuration is as follows

    type=KaKuSwitch; id=<switch_id>

The value of `<switch_id>` is the value you configured for this switch consisting of a letter and a number combination. A switch with an id `A3`  can be configured with:

    Switch Lamp {nodo="type=KaKuSwitch; id=A3"}

### NewKaKu binding ###

With this binding it is possible to bind a switch to a *Klik-Aan-Klik-Uit* device using the new, self learning, encoding scheme. The syntax of the binding configuration is as follows:

    type=NewKaKuSwitch; id=<switch_id>

The value of `<switch_id>` is the value you configured for this switch. This can be either a number between 0 and 255 or a hexadecimal value. This switch type also supports dimmers, so a dimmer with an id `100` can be configured with:

    Dimmer Light_Dinner <slider>  {nodo="type=NewKaKuSwitch; id=100"}

### Variable binding ###

With this binding it is possible to bind a Nodo variable to a value number or a string. A Nodo variable is always numeric. 
The syntax of this binding configuration is as follows:

    type=Variable; id=<variable_id>; unit=<unit_id>

The value of the '<variable_id>' is the identification number of the variable and the value of `unit_id` is the identification number of the Nodo device. If you have a small Nodo device with an id 3, measuring your power consumption in variable 1, then the binding can be configured with:

    Number Power_Consumption {nodo="type=Variable; id=1; unit=3"}

### Activity binding ###

With this binding it is possible to send commands to a Nodo device, it is therefore the most flexible of all the binding. This binding uses the mapping feature of OpenHAB to execute a command. Therefore every command must have key value which also serves as a key in the mapping.The syntax of this binding configuration is as follows:

    type=Activity; key1=<command1>|<command2>|etc; key2=<command3>|<command4>|etc

The value of the `key` is used in the OpenHAB mapping configurations, the right side is a list of commands that must be send to the Nodo device. You can specify more than one command by separating them with the `|` symbol.

To use a switch for sending user events to your Nodo device you can use the following example:

    Switch User_Button  { nodo="type=Activity; ON=UserEventSend 10,11; OFF=UserEventSend 10,12"}

When the switch changes to the `ON` state the command `UserEventSend 10,11` is send to the Nodo device and when the switch changes to the `OFF` state the command `UserEventSend 10,12` is send to the Nodo device.

This mechanisme can also be used to create a button which executes a number of commands. In this example a button is created which turns three lights off. The switch binding is as follows:

    Switch Lights_Off  {nodo="type=Activity; ON=KaKuSend L1,Off|NewKaKuSend 100,Off|NewKaKuSend 101,Off"}

And the button is created in the sitemap file with the following configuration:

    Switch item=Lights_Off label="Lights Off" mappings=[ON="All Off"]

# Future developments #

* Binding showing the status of the Nodo connection.
* Connect multiple id's to a NewKaKu device. This way the status is also updated if someone dared to use a physical switch.
* Provide special support for RGB Leds.
* Binding for displaying all the Nodo messages and events 

 
[Markdown Reference](https://bitbucket.org/tutorials/markdowndemo)