/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.nodo.internal;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openhab.binding.nodo.NodoBindingProvider;
import org.openhab.binding.nodo.internal.model.NodoEventListener;
import org.openhab.binding.nodo.internal.model.NodoResource;
import org.openhab.binding.nodo.internal.model.events.NodoEvent;
import org.openhab.binding.nodo.internal.nodo.NodoServer;
import org.openhab.core.binding.AbstractBinding;
import org.openhab.core.library.types.DateTimeType;
import org.openhab.core.library.types.DecimalType;
import org.openhab.core.library.types.IncreaseDecreaseType;
import org.openhab.core.library.types.OnOffType;
import org.openhab.core.library.types.PercentType;
import org.openhab.core.library.types.StringType;
import org.openhab.core.types.Command;
import org.openhab.core.types.State;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	

/**
 * Active Binding which communicates with a Nodo (see www.nodo-domotica.nl for details).
 * 
 * @author Ricardo Belfor
 * @since 1.5.0
 */
public class NodoBinding extends AbstractBinding<NodoBindingProvider> implements ManagedService {

	private static final Logger logger =  LoggerFactory.getLogger(NodoBinding.class);
	
	private NodoServer nodoServer;
	private MessageLister eventLister = new MessageLister();
	private int nodoPort;
	private String nodoIP;
	private String nodoPassword;
	private int serverPort;
	private Map<String, State> itemStates = new HashMap<>();
	
	/**
	 * Create a new Nodo connection and register this class as event listener.
	 * 
	 * @see org.openhab.core.binding.AbstractBinding#activate()
	 */
	@Override
	public void activate() {
		super.activate();
	}

	/**
	 * Destroys the Nodo connection and remove this class as an event listener.
	 */
	@Override
	public void deactivate() {
		super.deactivate();
		if ( nodoServer != null) {
			nodoServer.removeEventListener(eventLister);
			nodoServer.stop();
			nodoServer = null;
		}
	}

	@Override
	public void updated(Dictionary<String, ?> config) throws ConfigurationException {
		if (config != null) {
			parseConfiguration(config);
			startNodoServer();
		}
	}

	private void parseConfiguration(Dictionary<String, ?> config)
			throws ConfigurationException {

		Enumeration<String> keys = config.keys();
		while (keys.hasMoreElements()) {

			String key = (String) keys.nextElement();
			String value = (String) config.get(key);

			if ("server.port".equalsIgnoreCase(key)) {
				serverPort = parseConfigurationInt(key, value); 
				continue;
			}

			if ("nodo.port".equalsIgnoreCase(key)) {
				nodoPort = parseConfigurationInt(key, value); 
				continue;
			}
			if ("nodo.ip".equalsIgnoreCase(key)) {
				nodoIP = value; 
				continue;
			}

			if ("nodo.password".equalsIgnoreCase(key)) {
				nodoPassword = value; 
				continue;
			}
		}
	}

	private int parseConfigurationInt(String key, String value) throws ConfigurationException {
		try {
			return Integer.parseInt(value); 
		} catch (NumberFormatException e) {
			throw new ConfigurationException(key, e.getLocalizedMessage() );
		}
	}
	
	private void startNodoServer() {
		if ( nodoServer == null) {
			nodoServer = new NodoServer(serverPort, nodoIP, nodoPort, nodoPassword );
			nodoServer.addEventListener(eventLister);
	
			try {
				nodoServer.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private List<NodoBindingProvider> getProvidersForItemName(String itemName) {
		List<NodoBindingProvider> providers = new ArrayList<NodoBindingProvider>();
		for (NodoBindingProvider provider : this.providers) {
			if (provider.getItemNames().contains(itemName)) {
				providers.add(provider);
			}
		}
		return providers;
	}	

	private List<NodoBindingConfig> getBindingConfigsForEvent(NodoEvent event) {
		ArrayList<NodoBindingConfig> configs = new ArrayList<>();
		for (NodoBindingProvider provider : this.providers) {
			configs.addAll( provider.getNodoBindingConfigById( event.getId(), event.getUnit() ) );
		}
		return configs;
	}	

	@Override
	protected void internalReceiveCommand(String itemName, Command command) {
		for (NodoBindingProvider provider : getProvidersForItemName(itemName)) {
			NodoBindingConfig config = provider.getNodoBindingConfigByName(itemName);
			
			logger.debug("Nodo resource type: {}", config.getResourceType() );

			switch	 ( config.getResourceType() ) {
			case KAKU_SWITCH:
				handleKaKuSwitch(itemName, config, command);
				break;
			case NEW_KAKU_SWITCH:
				handleNewKaKuSwitch(itemName, config, command);
				break;
			case ACTIVITY:
				nodoServer.sendActivityCommand(command, config);
				break;
			default:
				break;
			}
		}
	}

	private void handleNewKaKuSwitch(String itemName, NodoBindingConfig config, Command command) {
		
		if ( !nodoServer.isConnected() ) {
			return;
		}
		
		PercentType itemState = (PercentType) getStoredNewKaKuState(itemName);
		State newState = computeNewKaKuState(command, (PercentType) itemState);

		if ( newState != null) {
			nodoServer.sendNewKaKuCommand(config, newState);
			eventPublisher.postUpdate(itemName, newState );
			itemStates.put(itemName, newState);
		}
	}

	private State getStoredNewKaKuState(String itemName) {
		State state = itemStates.get(itemName);
		if ( state == null) {
			state = new PercentType(0);
		} else if ( state instanceof OnOffType ) {
			state = new PercentType(state.equals(OnOffType.ON) ? 100 : 0 );
		}
		return state;
	}

	private State computeNewKaKuState(Command command, PercentType itemState) {

		if ( command instanceof PercentType ) {
			BigDecimal bigDecimalValue = ((PercentType) command).toBigDecimal();
			if ( bigDecimalValue.intValue() != 0) {
				return new PercentType( bigDecimalValue );
			} else {
				return OnOffType.OFF;
			}
		}
		
		if ( command instanceof OnOffType ) {
			return command.equals( OnOffType.OFF ) ? OnOffType.OFF : OnOffType.ON;
		}
		
		if ( command.equals(IncreaseDecreaseType.INCREASE ) ) {
			if ( itemState.intValue() < 100 ) {
				return new PercentType( itemState.intValue() + 10);
			}
		} else if ( command.equals(IncreaseDecreaseType.DECREASE ) ) {
			if ( itemState.intValue() > 0 ) {
				int value = itemState.intValue() - 10;
				if ( value <= 0 ) {
					return OnOffType.OFF;
				} else {
					return new PercentType( value);
				}
			}
		} 
		
		return null;
	}	

	private void handleKaKuSwitch(String itemName, NodoBindingConfig config,
			Command command) {

		if ( nodoServer.isConnected() ) {
			nodoServer.sendKaKuCommand(config,(State) command);
		}
		
	}

	@Override
	protected void internalReceiveUpdate(String itemName, State newState) {
		// TODO Auto-generated method stub
		super.internalReceiveUpdate(itemName, newState);
	}
	
	@SuppressWarnings("unchecked")
	private <R extends NodoResource> R findResource(String id, List<R> resources) {
		for (NodoResource resource : resources) {
			if (resource.getId().equals(id)) {
				return (R) resource;
			}
		}
		return null;
	}
	
	/**
	 * Creates an openHAB {@link State} in accordance to the given {@code dataType}. Currently
	 * {@link Date} and {@link BigDecimal} are handled explicitly. All other {@code dataTypes}
	 * are mapped to {@link StringType}.
	 * 
	 * @param dataType
	 * @param propertyValue
	 * 
	 * @return the new {@link State} in accordance to {@code dataType}. Will never be {@code null}.
	 */
	private State createState(Class<?> dataType, Object propertyValue) {
		if (Date.class.isAssignableFrom(dataType)) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime((Date) propertyValue);
			return new DateTimeType(calendar);
		} else if (BigDecimal.class.isAssignableFrom(dataType)) {
			return new DecimalType((BigDecimal) propertyValue);
		} else {
			return new StringType(propertyValue.toString());
		}
	}
	
	private class MessageLister implements NodoEventListener {

		@Override
		public void eventReceived(NodoEvent event) {

			logger.debug("Event Received: " + event.toString());

			for (NodoBindingConfig provider : getBindingConfigsForEvent(event)) {
				eventPublisher.postUpdate(provider.getBindingName(), event.getState() );
			}
		}
	}

	@Override
	public void addBindingProvider(NodoBindingProvider provider) {
		super.addBindingProvider(provider);
	}

	@Override
	public void removeBindingProvider(NodoBindingProvider provider) {
		super.removeBindingProvider(provider);
	}
}
