//
// TODO event=Message 6,0: Unable to establish TCP/IP connection.
//
grammar NodoEvent;

@header
{
package org.openhab.binding.nodo.internal.antlr.output;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.MessageFormat;

import org.openhab.binding.nodo.internal.model.events.*;
}

@lexer::header
{
package org.openhab.binding.nodo.internal.antlr.output;
}


@members
{
	private static final Logger logger = LoggerFactory.getLogger(NodoEventParser.class);
	private static final String LOG_ERROR_PARSING_EVENT = "Error parsing event ''{0}''";

	public static NodoEvent parse(String eventString, int unit)
	{               
		NodoEventLexer lexer = new NodoEventLexer( new ANTLRStringStream( eventString ) );
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		NodoEventParser parser = new NodoEventParser(tokens);
		
		try {
			NodoEvent nodoEvent = parser.expression();
			if ( nodoEvent != null) {
				nodoEvent.setUnit(unit);
			}
			nodoEvent.setUnit(unit);
			return nodoEvent;
		} catch (RecognitionException e) {
			// Ignore unknown events, but do log them! 
			logger.error(MessageFormat.format(LOG_ERROR_PARSING_EVENT, eventString), e);
			return null;
		}
	}
}

expression returns  [NodoEvent value]
	:	b=bootExpression
	{
		$value = $b.value;
	}
	|	k=kakuExpression
	{
		$value = $k.value;
	}
	|	n=newKakuExpression
	{
		$value = $n.value; 
	}
	|	v=variableExpression
 	{
		$value = $v.value;
	}
	|	u=userEventExpression
	{
		$value = $u.value;
	}
	;
	
bootExpression returns  [NodoEvent value]
	:	BOOT i=INT
	{
		return new BootEvent( $i.text );
	}
	;	

kakuExpression returns  [NodoEvent value]
	:	KAKU i=(ID | HEX ) COMMA s=(ON_STATE | OFF_STATE)
	{
		return new KaKuEvent( $i.text, $s.text );
	}
	;
	
newKakuExpression returns  [NodoEvent value]
	:	NEW_KAKU i=(HEX | INT ) COMMA s=(ON_STATE | OFF_STATE | INT)
	{
		return new NewKaKuEvent( $i.text, $s.text );
	}
	;
variableExpression  returns  [NodoEvent value] 
	:	VARIABLE i=INT COMMA v=(FLOAT | INT )
	{
		return new VariableEvent( $i.text, Double.parseDouble($v.text) );
	}	
	;

userEventExpression  returns  [NodoEvent value] 
	:	USER_EVENT id1=INT COMMA id2=INT
	{
		return new UserEventEvent( $id1.text + ","  + $id2.text );
	}	
	;

VARIABLE	: 	'Variable';	
BOOT		:	'Boot';
KAKU		:	'Kaku';
NEW_KAKU	:	'NewKAKU';
USER_EVENT	:	'UserEvent';
ON_STATE	:	'On';
OFF_STATE	:	'Off';

COMMA 	:	 ',';

ID  :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;
	
INT :	'0'..'9'+
    ;

FLOAT
    :   ('0'..'9')+ '.' ('0'..'9')* 
    |   '.' ('0'..'9')+ 
    |   ('0'..'9')+
    ;

HEX 	:	 '0x' HexDigit+;

fragment
HexDigit : ('0'..'9'|'a'..'f'|'A'..'F') ;

WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {$channel=HIDDEN;}
    ;