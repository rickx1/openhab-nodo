/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */

package org.openhab.binding.nodo.internal.model.events;

import org.openhab.core.types.State;
import org.openhab.core.types.UnDefType;

/**
 * @author Ricardo Belfor
 *
 */
public class UserEventEvent extends NodoEvent {

	public UserEventEvent(String id) {
		super(id);
	}

	@Override
	public State getState() {
		return UnDefType.UNDEF;
	}

	@Override
	public String toString() {
		return "UserEventEvent [getId()=" + getId() + ", getUnit()="
				+ getUnit() + "]";
	}
}
