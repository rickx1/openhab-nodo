// $ANTLR 3.5 /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g 2014-12-28 07:40:08

package org.openhab.binding.nodo.internal.antlr.output;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class NodoEventLexer extends Lexer {
	public static final int EOF=-1;
	public static final int BOOT=4;
	public static final int COMMA=5;
	public static final int FLOAT=6;
	public static final int HEX=7;
	public static final int HexDigit=8;
	public static final int ID=9;
	public static final int INT=10;
	public static final int KAKU=11;
	public static final int NEW_KAKU=12;
	public static final int OFF_STATE=13;
	public static final int ON_STATE=14;
	public static final int USER_EVENT=15;
	public static final int VARIABLE=16;
	public static final int WS=17;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public NodoEventLexer() {} 
	public NodoEventLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public NodoEventLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g"; }

	// $ANTLR start "VARIABLE"
	public final void mVARIABLE() throws RecognitionException {
		try {
			int _type = VARIABLE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:106:10: ( 'Variable' )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:106:13: 'Variable'
			{
			match("Variable"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VARIABLE"

	// $ANTLR start "BOOT"
	public final void mBOOT() throws RecognitionException {
		try {
			int _type = BOOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:107:7: ( 'Boot' )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:107:9: 'Boot'
			{
			match("Boot"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOT"

	// $ANTLR start "KAKU"
	public final void mKAKU() throws RecognitionException {
		try {
			int _type = KAKU;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:108:7: ( 'Kaku' )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:108:9: 'Kaku'
			{
			match("Kaku"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "KAKU"

	// $ANTLR start "NEW_KAKU"
	public final void mNEW_KAKU() throws RecognitionException {
		try {
			int _type = NEW_KAKU;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:109:10: ( 'NewKAKU' )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:109:12: 'NewKAKU'
			{
			match("NewKAKU"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEW_KAKU"

	// $ANTLR start "USER_EVENT"
	public final void mUSER_EVENT() throws RecognitionException {
		try {
			int _type = USER_EVENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:110:12: ( 'UserEvent' )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:110:14: 'UserEvent'
			{
			match("UserEvent"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "USER_EVENT"

	// $ANTLR start "ON_STATE"
	public final void mON_STATE() throws RecognitionException {
		try {
			int _type = ON_STATE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:111:10: ( 'On' )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:111:12: 'On'
			{
			match("On"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ON_STATE"

	// $ANTLR start "OFF_STATE"
	public final void mOFF_STATE() throws RecognitionException {
		try {
			int _type = OFF_STATE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:112:11: ( 'Off' )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:112:13: 'Off'
			{
			match("Off"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OFF_STATE"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:114:8: ( ',' )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:114:11: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:116:5: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:116:7: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:116:31: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:119:5: ( ( '0' .. '9' )+ )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:119:7: ( '0' .. '9' )+
			{
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:119:7: ( '0' .. '9' )+
			int cnt2=0;
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt2 >= 1 ) break loop2;
					EarlyExitException eee = new EarlyExitException(2, input);
					throw eee;
				}
				cnt2++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "FLOAT"
	public final void mFLOAT() throws RecognitionException {
		try {
			int _type = FLOAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:123:5: ( ( '0' .. '9' )+ '.' ( '0' .. '9' )* | '.' ( '0' .. '9' )+ | ( '0' .. '9' )+ )
			int alt7=3;
			alt7 = dfa7.predict(input);
			switch (alt7) {
				case 1 :
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:123:9: ( '0' .. '9' )+ '.' ( '0' .. '9' )*
					{
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:123:9: ( '0' .. '9' )+
					int cnt3=0;
					loop3:
					while (true) {
						int alt3=2;
						int LA3_0 = input.LA(1);
						if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
							alt3=1;
						}

						switch (alt3) {
						case 1 :
							// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt3 >= 1 ) break loop3;
							EarlyExitException eee = new EarlyExitException(3, input);
							throw eee;
						}
						cnt3++;
					}

					match('.'); 
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:123:25: ( '0' .. '9' )*
					loop4:
					while (true) {
						int alt4=2;
						int LA4_0 = input.LA(1);
						if ( ((LA4_0 >= '0' && LA4_0 <= '9')) ) {
							alt4=1;
						}

						switch (alt4) {
						case 1 :
							// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop4;
						}
					}

					}
					break;
				case 2 :
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:124:9: '.' ( '0' .. '9' )+
					{
					match('.'); 
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:124:13: ( '0' .. '9' )+
					int cnt5=0;
					loop5:
					while (true) {
						int alt5=2;
						int LA5_0 = input.LA(1);
						if ( ((LA5_0 >= '0' && LA5_0 <= '9')) ) {
							alt5=1;
						}

						switch (alt5) {
						case 1 :
							// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt5 >= 1 ) break loop5;
							EarlyExitException eee = new EarlyExitException(5, input);
							throw eee;
						}
						cnt5++;
					}

					}
					break;
				case 3 :
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:125:9: ( '0' .. '9' )+
					{
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:125:9: ( '0' .. '9' )+
					int cnt6=0;
					loop6:
					while (true) {
						int alt6=2;
						int LA6_0 = input.LA(1);
						if ( ((LA6_0 >= '0' && LA6_0 <= '9')) ) {
							alt6=1;
						}

						switch (alt6) {
						case 1 :
							// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt6 >= 1 ) break loop6;
							EarlyExitException eee = new EarlyExitException(6, input);
							throw eee;
						}
						cnt6++;
					}

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FLOAT"

	// $ANTLR start "HEX"
	public final void mHEX() throws RecognitionException {
		try {
			int _type = HEX;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:128:6: ( '0x' ( HexDigit )+ )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:128:9: '0x' ( HexDigit )+
			{
			match("0x"); 

			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:128:14: ( HexDigit )+
			int cnt8=0;
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( ((LA8_0 >= '0' && LA8_0 <= '9')||(LA8_0 >= 'A' && LA8_0 <= 'F')||(LA8_0 >= 'a' && LA8_0 <= 'f')) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt8 >= 1 ) break loop8;
					EarlyExitException eee = new EarlyExitException(8, input);
					throw eee;
				}
				cnt8++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HEX"

	// $ANTLR start "HexDigit"
	public final void mHexDigit() throws RecognitionException {
		try {
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:132:10: ( ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' ) )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HexDigit"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:133:5: ( ( ' ' | '\\t' | '\\r' | '\\n' ) )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:133:9: ( ' ' | '\\t' | '\\r' | '\\n' )
			{
			if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	@Override
	public void mTokens() throws RecognitionException {
		// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:8: ( VARIABLE | BOOT | KAKU | NEW_KAKU | USER_EVENT | ON_STATE | OFF_STATE | COMMA | ID | INT | FLOAT | HEX | WS )
		int alt9=13;
		alt9 = dfa9.predict(input);
		switch (alt9) {
			case 1 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:10: VARIABLE
				{
				mVARIABLE(); 

				}
				break;
			case 2 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:19: BOOT
				{
				mBOOT(); 

				}
				break;
			case 3 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:24: KAKU
				{
				mKAKU(); 

				}
				break;
			case 4 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:29: NEW_KAKU
				{
				mNEW_KAKU(); 

				}
				break;
			case 5 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:38: USER_EVENT
				{
				mUSER_EVENT(); 

				}
				break;
			case 6 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:49: ON_STATE
				{
				mON_STATE(); 

				}
				break;
			case 7 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:58: OFF_STATE
				{
				mOFF_STATE(); 

				}
				break;
			case 8 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:68: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 9 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:74: ID
				{
				mID(); 

				}
				break;
			case 10 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:77: INT
				{
				mINT(); 

				}
				break;
			case 11 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:81: FLOAT
				{
				mFLOAT(); 

				}
				break;
			case 12 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:87: HEX
				{
				mHEX(); 

				}
				break;
			case 13 :
				// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:1:91: WS
				{
				mWS(); 

				}
				break;

		}
	}


	protected DFA7 dfa7 = new DFA7(this);
	protected DFA9 dfa9 = new DFA9(this);
	static final String DFA7_eotS =
		"\1\uffff\1\4\3\uffff";
	static final String DFA7_eofS =
		"\5\uffff";
	static final String DFA7_minS =
		"\2\56\3\uffff";
	static final String DFA7_maxS =
		"\2\71\3\uffff";
	static final String DFA7_acceptS =
		"\2\uffff\1\2\1\1\1\3";
	static final String DFA7_specialS =
		"\5\uffff}>";
	static final String[] DFA7_transitionS = {
			"\1\2\1\uffff\12\1",
			"\1\3\1\uffff\12\1",
			"",
			"",
			""
	};

	static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
	static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
	static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
	static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
	static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
	static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
	static final short[][] DFA7_transition;

	static {
		int numStates = DFA7_transitionS.length;
		DFA7_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
		}
	}

	protected class DFA7 extends DFA {

		public DFA7(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 7;
			this.eot = DFA7_eot;
			this.eof = DFA7_eof;
			this.min = DFA7_min;
			this.max = DFA7_max;
			this.accept = DFA7_accept;
			this.special = DFA7_special;
			this.transition = DFA7_transition;
		}
		@Override
		public String getDescription() {
			return "122:1: FLOAT : ( ( '0' .. '9' )+ '.' ( '0' .. '9' )* | '.' ( '0' .. '9' )+ | ( '0' .. '9' )+ );";
		}
	}

	static final String DFA9_eotS =
		"\1\uffff\6\10\2\uffff\1\25\1\uffff\1\25\1\uffff\5\10\1\33\1\10\2\uffff"+
		"\5\10\1\uffff\1\42\1\10\1\44\1\45\2\10\1\uffff\1\10\2\uffff\6\10\1\57"+
		"\1\10\1\61\1\uffff\1\10\1\uffff\1\63\1\uffff";
	static final String DFA9_eofS =
		"\64\uffff";
	static final String DFA9_minS =
		"\1\11\1\141\1\157\1\141\1\145\1\163\1\146\2\uffff\1\56\1\uffff\1\56\1"+
		"\uffff\1\162\1\157\1\153\1\167\1\145\1\60\1\146\2\uffff\1\151\1\164\1"+
		"\165\1\113\1\162\1\uffff\1\60\1\141\2\60\1\101\1\105\1\uffff\1\142\2\uffff"+
		"\1\113\1\166\1\154\1\125\2\145\1\60\1\156\1\60\1\uffff\1\164\1\uffff\1"+
		"\60\1\uffff";
	static final String DFA9_maxS =
		"\1\172\1\141\1\157\1\141\1\145\1\163\1\156\2\uffff\1\170\1\uffff\1\71"+
		"\1\uffff\1\162\1\157\1\153\1\167\1\145\1\172\1\146\2\uffff\1\151\1\164"+
		"\1\165\1\113\1\162\1\uffff\1\172\1\141\2\172\1\101\1\105\1\uffff\1\142"+
		"\2\uffff\1\113\1\166\1\154\1\125\2\145\1\172\1\156\1\172\1\uffff\1\164"+
		"\1\uffff\1\172\1\uffff";
	static final String DFA9_acceptS =
		"\7\uffff\1\10\1\11\1\uffff\1\13\1\uffff\1\15\7\uffff\1\14\1\12\5\uffff"+
		"\1\6\6\uffff\1\7\1\uffff\1\2\1\3\11\uffff\1\4\1\uffff\1\1\1\uffff\1\5";
	static final String DFA9_specialS =
		"\64\uffff}>";
	static final String[] DFA9_transitionS = {
			"\2\14\2\uffff\1\14\22\uffff\1\14\13\uffff\1\7\1\uffff\1\12\1\uffff\1"+
			"\11\11\13\7\uffff\1\10\1\2\10\10\1\3\2\10\1\4\1\6\5\10\1\5\1\1\4\10\4"+
			"\uffff\1\10\1\uffff\32\10",
			"\1\15",
			"\1\16",
			"\1\17",
			"\1\20",
			"\1\21",
			"\1\23\7\uffff\1\22",
			"",
			"",
			"\1\12\1\uffff\12\13\76\uffff\1\24",
			"",
			"\1\12\1\uffff\12\13",
			"",
			"\1\26",
			"\1\27",
			"\1\30",
			"\1\31",
			"\1\32",
			"\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
			"\1\34",
			"",
			"",
			"\1\35",
			"\1\36",
			"\1\37",
			"\1\40",
			"\1\41",
			"",
			"\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
			"\1\43",
			"\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
			"\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
			"\1\46",
			"\1\47",
			"",
			"\1\50",
			"",
			"",
			"\1\51",
			"\1\52",
			"\1\53",
			"\1\54",
			"\1\55",
			"\1\56",
			"\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
			"\1\60",
			"\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
			"",
			"\1\62",
			"",
			"\12\10\7\uffff\32\10\4\uffff\1\10\1\uffff\32\10",
			""
	};

	static final short[] DFA9_eot = DFA.unpackEncodedString(DFA9_eotS);
	static final short[] DFA9_eof = DFA.unpackEncodedString(DFA9_eofS);
	static final char[] DFA9_min = DFA.unpackEncodedStringToUnsignedChars(DFA9_minS);
	static final char[] DFA9_max = DFA.unpackEncodedStringToUnsignedChars(DFA9_maxS);
	static final short[] DFA9_accept = DFA.unpackEncodedString(DFA9_acceptS);
	static final short[] DFA9_special = DFA.unpackEncodedString(DFA9_specialS);
	static final short[][] DFA9_transition;

	static {
		int numStates = DFA9_transitionS.length;
		DFA9_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA9_transition[i] = DFA.unpackEncodedString(DFA9_transitionS[i]);
		}
	}

	protected class DFA9 extends DFA {

		public DFA9(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 9;
			this.eot = DFA9_eot;
			this.eof = DFA9_eof;
			this.min = DFA9_min;
			this.max = DFA9_max;
			this.accept = DFA9_accept;
			this.special = DFA9_special;
			this.transition = DFA9_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( VARIABLE | BOOT | KAKU | NEW_KAKU | USER_EVENT | ON_STATE | OFF_STATE | COMMA | ID | INT | FLOAT | HEX | WS );";
		}
	}

}
