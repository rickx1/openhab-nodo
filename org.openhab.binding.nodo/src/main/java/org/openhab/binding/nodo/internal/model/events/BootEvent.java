/**
 * 
 */
package org.openhab.binding.nodo.internal.model.events;

import org.openhab.core.library.types.OnOffType;
import org.openhab.core.types.State;

/**
 * This class represents a boot event sent by the Nodo.
 * 
 * @author Ricardo Belfor
 *
 */
public class BootEvent extends NodoEvent {

	public BootEvent(String unitId) {
		super(unitId);
	}

	@Override
	public String toString() {
		return "BootEvent [getId()=" + getId() + "]";
	}

	@Override
	public State getState() {
		return OnOffType.ON;
	}
}
