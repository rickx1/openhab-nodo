/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.nodo.internal;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.openhab.binding.nodo.internal.model.NodoResourceType;
import org.openhab.model.item.binding.BindingConfigParseException;

/**
 * This class parses the nodo configuration string into key/value pairs.
 * 
 * @author Ricardo Belfor
 *
 */
public class ConfigParser {

	private static final String PAIR_DELIMITERS = "{};";
	private static final String KEY_VALUE_DELIMITER = "=";
	private static final String UNKNOWN_TYPE_EXCEPTION = "Error parsing ''{0}'', unknown type ''{1}''";
	private static final String MISSING_TYPE_EXCEPTION = "Error parsing ''{0}'', missing type";
	
	private static final String TYPE_KEY = "type";

	private Map<String,String> configMap = new HashMap<>();
	private final String config;
	
	public ConfigParser(String config) throws BindingConfigParseException {
		this.config = config;
	}
	
	public NodoConfig parse() throws BindingConfigParseException {
		
		StringTokenizer tokenizer = new StringTokenizer(config, PAIR_DELIMITERS);
		
		while(tokenizer.hasMoreTokens()) { 
			parseKeyValuePair(tokenizer.nextToken() );
		}
		
		NodoConfig bindingConfig = new NodoConfig( getType(), configMap );
		return bindingConfig;
	}

	private void parseKeyValuePair(String keyValuePair) throws BindingConfigParseException {
		StringTokenizer tokenizer = new StringTokenizer(keyValuePair, KEY_VALUE_DELIMITER );
		String key = tokenizer.nextToken().trim();
		if (!tokenizer.hasMoreTokens() ) {
			throw new BindingConfigParseException(key + " has no value");
		} else {
			configMap.put(key, tokenizer.nextToken().trim() );
		}
	}

	private NodoResourceType getType() throws BindingConfigParseException {
		String type = configMap.get(TYPE_KEY);
		
		if ( type == null) {
			throw new BindingConfigParseException( MessageFormat.format(MISSING_TYPE_EXCEPTION, config) );
		}

		switch (type ) {
		case "Activity": 
			return NodoResourceType.ACTIVITY;
		case "KaKuSwitch":
			return NodoResourceType.KAKU_SWITCH;
		case "NewKaKuSwitch":
			return NodoResourceType.NEW_KAKU_SWITCH;
		case "Variable":
			return NodoResourceType.VARIABLE;
		default:
			throw new BindingConfigParseException( MessageFormat.format(UNKNOWN_TYPE_EXCEPTION, config, type) );
		}
	}
}