/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.nodo.internal.nodo;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.openhab.binding.nodo.internal.NodoBinding;
import org.openhab.binding.nodo.internal.NodoBindingConfig;
import org.openhab.binding.nodo.internal.antlr.output.NodoEventParser;
import org.openhab.binding.nodo.internal.model.NodoEventListener;
import org.openhab.binding.nodo.internal.model.events.NodoEvent;
import org.openhab.binding.nodo.internal.model.events.VariableEvent;
import org.openhab.core.library.types.OnOffType;
import org.openhab.core.library.types.PercentType;
import org.openhab.core.types.Command;
import org.openhab.core.types.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fi.iki.elonen.NanoHTTPD;

/**
 * This class is the interface between the Nodo device and the binding. It
 * listens for events from the Nodo device and sends commands to the Nodo device
 * using the Nodo connection.
 * 
 * @author Ricardo Belfor
 * 
 */
public class NodoServer extends NanoHTTPD {
	
	private static final Logger logger =  LoggerFactory.getLogger(NodoBinding.class);

	private static final String EVENT_KEY = "event";
	private static final String COOKIE_KEY = "cookie";

	private static final String KAKU_SEND_EVENT = "KAKUSend%20{0},{1}";
	private static final String NEWKAKU_SEND_EVENT = "NewKAKUSend%20{0},{1}";

	private static List<NodoEventListener> listeners = new ArrayList<NodoEventListener>();
	
	private final NodoConsole console = new NodoConsole();
	private final NodoConnection connection;
	private final ExecutorService executorService = Executors.newSingleThreadExecutor();
	
    public NodoServer(int serverPort, String nodoIP, int nodoPort, String nodoPassword) {
        super(serverPort);
        connection = new NodoConnection(serverPort, nodoIP, nodoPort, nodoPassword);
    }

    public boolean isConnected() {
    	return connection.isConnected();
    }
        
	public synchronized void addEventListener(NodoEventListener listener) {
		listeners.add(listener);
	}

	public synchronized void removeEventListener(NodoEventListener listener) {
		listeners.remove(listener);
	}
    
    public Response serve(String uri, Method method, Map<String, String> header, Map<String, String> parms, Map<String, String> files) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<head><title>Nodo Dispatcher</title></head>");
        sb.append("<body>");
//        sb.append("<h1>Response</h1>");
//        sb.append("<p><blockquote><b>URI -</b> ").append(String.valueOf(uri)).append("<br />");
//        sb.append("<b>Method -</b> ").append(String.valueOf(method)).append("</blockquote></p>");
//        sb.append("<h3>Headers</h3><p><blockquote>").append(String.valueOf(header)).append("</blockquote></p>");
//        sb.append("<h3>Parms</h3><p><blockquote>").append(String.valueOf(parms)).append("</blockquote></p>");
//        sb.append("<h3>Files</h3><p><blockquote>").append(String.valueOf(files)).append("</blockquote></p>");
        sb.append("</body>");
        sb.append("</html>");
        
        logger.debug("Parameters: {})", String.valueOf(parms) );
        
        if ( parms.containsKey(COOKIE_KEY) ) {
        	setHashKey(parms);
        }
        
        if ( parms.containsKey(EVENT_KEY) ) {
        	handleEvent(parms);
        }
   
        if ( "/command".equals(uri) ) {
        	NodoEvent nodoEvent = new VariableEvent("1", 42);
        	fireEvent(nodoEvent);
        }
        
        if ("/console".equals(uri) ) {
        	return console.execute(connection, parms);
        }
        return new Response(sb.toString());
    }

	private void handleEvent(Map<String, String> parms) {
		String event = parms.get(EVENT_KEY);
		int unit = getUnitFromParams(parms);
		NodoEvent nodoEvent = NodoEventParser.parse(event, unit);
		if ( nodoEvent != null) {
			logger.debug("Event: {}", nodoEvent.toString());
			fireEvent(nodoEvent);
		}
	}

	private int getUnitFromParams(Map<String, String> parms) {
		String unitString = parms.get("unit");
		try {
			return Integer.parseInt(unitString);
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	private void setHashKey(Map<String, String> parms) {
		String cookie = parms.get(COOKIE_KEY);
		connection.setHashKey(cookie);
		
	}

	private void fireEvent(NodoEvent nodoEvent) {
		for ( NodoEventListener listener : listeners ) {
			listener.eventReceived(nodoEvent);
		}
	}
    
	/**
	 * Sends a command to a new KaKu device.
	 * 
	 * @param config the configuration of the device
	 * @param state the desired state
	 */
	public void sendNewKaKuCommand(NodoBindingConfig config, State state) {
		String command = MessageFormat.format(NEWKAKU_SEND_EVENT, 
				config.getResourceId(), 
				getNewKaKuCommandArg(state) );

 		executorService.submit( new NodoCommand(connection, command) );
	}

	/**
	 * Return the argument for the new KaKu switch based on the desired state. A percent
	 * value will be mapped to a value between 0 and 12.
	 * 
	 * @param state the desired state
	 * @return the command argument
	 */
	private String getNewKaKuCommandArg(State state) {
		String arg = null;
		
		if ( state instanceof PercentType ) {
			int percentValue = ((PercentType)state).intValue();
			arg = Integer.toString(12 * percentValue/100);
		} else if ( state.equals(OnOffType.OFF)) {
			arg = "Off";
		} else if ( state.equals(OnOffType.ON)) {
			arg = "On";
		}
		return arg;
	}

	public void sendKaKuCommand(NodoBindingConfig config, State state) {

		String command = MessageFormat.format(KAKU_SEND_EVENT, 
				config.getResourceId(), 
				state.equals(OnOffType.OFF) ? "Off" : "On");
		
		executorService.submit( new NodoCommand(connection, command) );
	}
	
	public void sendActivityCommand(Command command, NodoBindingConfig config) {
		Collection<String> activities = config.getActivityFor(command.toString() );
		for ( String activity : activities ) {
			executorService.submit( new NodoCommand(connection, activity) );
		}
	}
}
