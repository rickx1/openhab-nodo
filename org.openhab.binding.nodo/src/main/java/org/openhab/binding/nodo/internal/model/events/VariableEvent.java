/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */

package org.openhab.binding.nodo.internal.model.events;

import org.openhab.core.library.types.DecimalType;
import org.openhab.core.types.State;

/**
 * This class represents a variable event sent by the Nodo.
 * 
 * @author Ricardo Belfor
 *
 */
public class VariableEvent extends NodoEvent {

	private final double value;

	public VariableEvent(String id, double value) {
		super(id);
		this.value = value;
	}

	@Override
	public String toString() {
		return "VariableEvent [value=" + value + ", getId()=" + getId()
				+ ", getUnit()=" + getUnit() + "]";
	}

	public double getValue() {
		return value;
	}

	@Override
	public State getState() {
		return new DecimalType(value);
	}
}
