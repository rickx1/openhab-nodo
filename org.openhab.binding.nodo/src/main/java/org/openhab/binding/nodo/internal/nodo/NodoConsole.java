/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.nodo.internal.nodo;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.openhab.binding.nodo.internal.NodoActivator;

import fi.iki.elonen.NanoHTTPD.Response;

/**
 * This class implements the console provided by the Nodo server.
 *  
 * @author Ricardo Belfor
 *
 */
public class NodoConsole {

    public static final String MIME_HTML = "text/html";
    public static final String MIME_PLAINTEXT = "text/plain";

	public Response execute(NodoConnection connection, Map<String, String> parms) {
		
    	URL entry = NodoActivator.getContext().getBundle().getResource("/console.html");
    	try {
    		String htmlString = IOUtils.toString( entry.openStream() );
    		String command = parms.get("command");
    		String response = null;
    		
    		if ( command != null ) {
// TODO fix this    			
//	    		response = connection.executeCommand(command);
    		}
    		
			htmlString = StringUtils.replaceEach(htmlString, 
					new String[] { "$$command$$", "$$response$$" },
					new String[] { StringUtils.defaultString(command),
							StringUtils.defaultString(response) });
			
			return new Response(Response.Status.OK, MIME_HTML, htmlString );
		} catch (IOException e) {
			return new Response(Response.Status.INTERNAL_ERROR, MIME_PLAINTEXT, e.getLocalizedMessage() );
		}
	}
}
