/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */

package org.openhab.binding.nodo.internal.model.events;

import org.openhab.core.library.types.DecimalType;
import org.openhab.core.library.types.OnOffType;
import org.openhab.core.library.types.PercentType;
import org.openhab.core.types.State;

/**
 * This class represents a 'Klik-aan-Klik-uit' event sent by the Nodo.

 * @author Ricardo Belfor
 *
 */
public class NewKaKuEvent extends NodoEvent {

	private static final String NEW_KAKU_ON = "On";
	private static final String NEW_KAKU_OFF = "Off";
	
	private final String value;

	public NewKaKuEvent(String id, String value) {
		super(id);
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public State getState() {
		return value.equalsIgnoreCase(NEW_KAKU_ON) ? OnOffType.ON 
				: (value.equalsIgnoreCase(NEW_KAKU_OFF) ? OnOffType.OFF 
						: new PercentType( (Integer.parseInt(value) * 100)/16 ) );
	}

	@Override
	public String toString() {
		return "NewKaKuEvent [value=" + value + ", getState()=" + getState()
				+ ", getId()=" + getId() + "]";
	}

}
