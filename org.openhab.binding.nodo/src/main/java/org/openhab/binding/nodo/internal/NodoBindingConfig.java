/**
 * 
 */
package org.openhab.binding.nodo.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

import org.openhab.binding.nodo.internal.model.NodoResourceType;
import org.openhab.core.binding.BindingConfig;
import org.openhab.core.items.Item;

/**
 * @author Ricardo Belfor
 *
 */
public class NodoBindingConfig implements BindingConfig {
	
	private final NodoResourceType resourceType;
	private final String resourceId;
	private final String bindingName;
	private final Class<? extends Item> itemType;
	private final int unit;
	private final NodoConfig nodoConfig;
	
	/**
	 * Initializes the nodo binding configuration.
	 * 
	 * @param type
	 * @param nodoConfig
	 * @param item
	 */
	public NodoBindingConfig(NodoResourceType type, NodoConfig nodoConfig, Item item) {
		resourceType = type;
		this.nodoConfig = nodoConfig;
		itemType = item.getClass();
		bindingName = item.getName();
		resourceId = nodoConfig.getStringValue("id" );
		String stringUnit = nodoConfig.getStringValue("unit");
		unit = stringUnit != null ? Integer.parseInt( stringUnit ) : 0;
	}

	public NodoResourceType getResourceType() {
		return resourceType;
	}


	public String getResourceId() {
		return resourceId;
	}


	public String getBindingName() {
		return bindingName;
	}

	public Collection<String> getActivityFor(String name) {
		ArrayList<String> activities = new ArrayList<String>();
		String activitiesString = nodoConfig.getStringValue(name);
		if ( activitiesString != null )  {
			for (String activityString : activitiesString.split("\\|") ) {
				activities.add(activityString);
			}
		}
		return activities;
	}

	@Override
	public String toString() {
		return "NodoBindingConfig [resourceType=" + resourceType
				+ ", resourceId=" + resourceId + ", bindingName=" + bindingName
				+ ", itemType=" + itemType + ", unit=" + unit + "]";
	}

	public Class<? extends Item> getItemType() {
		return itemType;
	}

	public int getUnit() {
		return unit;
	}
}
