package org.openhab.binding.nodo.internal;

import java.util.Map;

import org.openhab.binding.nodo.internal.model.NodoResourceType;

public class NodoConfig {

	private final NodoResourceType type;
	private final Map<String, String> configMap;

	public NodoConfig(NodoResourceType type, Map<String, String> configMap) {
		this.type = type;
		this.configMap = configMap;
	}

	/**
	 * Returns the value of the key as string.
	 * 
	 * @param key the key
	 * @return the value
	 */
	public String getStringValue(String key) {
		return configMap.get(key);
	}

	public NodoResourceType getType() {
		return type;
	}
}
