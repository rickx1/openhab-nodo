/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.nodo.internal.nodo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.text.MessageFormat;

import org.apache.commons.lang.StringUtils;
import org.openhab.binding.nodo.internal.NodoBinding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class manages the connection to the Nodo device.
 *  
 * @author Ricardo Belfor
 *
 */
public class NodoConnection {

	private static final String FAKE_HASH_KEY = "172077e45f97c38310a153304aba795e";
	private static final String COMMAND_URL_FMT = "http://{0}:{1}/?event={2}&key={3}";

	private static final Logger logger =  LoggerFactory.getLogger(NodoBinding.class);
	
	private final String nodoIP;
	private final int nodoPort;
	private final String nodoPassword;

	private String currentHashKey;
	
	public NodoConnection(int serverPort, String nodoIP, int nodoPort, String nodoPassword) {
		super();
		this.nodoIP = nodoIP;
		this.nodoPort = nodoPort;
		this.nodoPassword = nodoPassword;

		if ( nodoPort == serverPort && "127.0.0.1".equals(nodoIP) ) {
			currentHashKey = FAKE_HASH_KEY;
		}
	}

	public void setHashKey(String cookie) {
		String hashKey = generateKey(cookie);
		if (hashKey != null) {
			currentHashKey = hashKey;
			logger.debug("Hash key: {}", hashKey);
		}
	}
	
    private String generateKey(String cookie) {
    	
		try {
	    	String key = cookie + ":" + nodoPassword;
	    	MessageDigest md5Digest = MessageDigest.getInstance("MD5");
			byte[] thedigest = md5Digest.digest( key.getBytes("UTF-8") );
			return convertToHexString(thedigest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
    }

	private String convertToHexString(byte[] thedigest) {
		StringBuffer hexString = new StringBuffer();
		for (int i=0;i<thedigest.length;i++) {
			String hex = Integer.toHexString(0xFF & thedigest[i]);
			if (hex.length() == 1) {
				hexString.append('0');
			}
			hexString.append(hex);	    
		}
		return hexString.toString();
	}
	
	/**
	 * Returns the nodo command URL for the specified event.
	 * 
	 * @param event the event to send.
	 * @return the command URL
	 */
	private String getCommandUrl(String event) {
		
		if ( currentHashKey == null ) {
			// TODO throw exception      s
		}
		String commandUrl = MessageFormat.format(COMMAND_URL_FMT, nodoIP,
				Integer.toString( nodoPort ), event, currentHashKey );
		
		commandUrl = StringUtils.replace(commandUrl, " ", "%20");
		logger.debug( commandUrl );
		return commandUrl;
	}
	
//	public String executeCommand(String command ) {
//		return getHTML( getCommandUrl(command) );
//	}
	
	public String getHTML(String urlToRead) {
	      URL url;
	      HttpURLConnection conn;
	      BufferedReader rd;
	      String line;
	      String result = "";
	      try {
	         url = new URL(urlToRead);
	         conn = (HttpURLConnection) url.openConnection();
	         conn.setRequestMethod("GET");
	         rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	         while ((line = rd.readLine()) != null) {
	            result += line;
	         }
	         rd.close();
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	      return result;
	   }

	public boolean isConnected() {
		return currentHashKey != null;
	}

	public int getNodoPort() {
		return nodoPort;
	}

	public String getCurrentHashKey() {
		return currentHashKey;
	}

	public String getNodoIP() {
		return nodoIP;
	}
}
