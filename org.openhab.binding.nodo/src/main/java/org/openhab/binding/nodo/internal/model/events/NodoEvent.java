/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.nodo.internal.model.events;

import org.openhab.core.types.State;


/**
 * @author Ricardo Belfor
 *
 */
public abstract class NodoEvent {
	
	private final String id;
	private int unit;
	
	public NodoEvent(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public abstract State getState();

	public int getUnit() {
		return unit;
	}

	public void setUnit(int unit) {
		this.unit = unit;
	}
	
	
//	private final String name;
//	private final NodoEventType eventType;
//	private final String value;
//
//	public NodoEvent(String name, NodoEventType eventType, String value) {
//		this.name = name;
//		this.eventType = eventType;
//		this.value = value;
//	}
//	
//	public String getName() {
//		return name;
//	}
//
//	public NodoEventType getEventType() {
//		return eventType;
//	}
//
//	public String getValue() {
//		return value;
//	}
}
