/**
 * Copyright (c) 2010-2015, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */

package org.openhab.binding.nodo.internal.nodo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.util.concurrent.Callable;

import org.apache.commons.lang.StringUtils;
import org.openhab.binding.nodo.internal.NodoBinding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ricardo Belfor
 *
 */
public class NodoCommand implements Callable<String> {

	private static final int CONNECTION_TIMEOUT = 5000;
	private static final Logger logger =  LoggerFactory.getLogger(NodoBinding.class);
	private static final String COMMAND_URL_FMT = "http://{0}:{1}/?event={2}&key={3}";

	private final NodoConnection nodoConnection;
	private final String command;

	public NodoCommand(NodoConnection nodoConnection, String command) {
		this.nodoConnection = nodoConnection;
		this.command = command;
	}

	@Override
	public String call() throws Exception {
		return getHTML( getCommandUrl(command) );
	}
	
	public String getHTML(String urlToRead) {
		URL url;
		HttpURLConnection conn;
		try {
			url = new URL(urlToRead);
			conn = (HttpURLConnection) url.openConnection();
			System.out.println( conn.getConnectTimeout() );
			conn.setConnectTimeout(CONNECTION_TIMEOUT);
			conn.setRequestMethod("GET");
			return getResult( conn.getInputStream() );
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
   }

	private String getResult(InputStream inputStream) throws IOException {
		BufferedReader rd = null;
		try {
			rd = new BufferedReader(new InputStreamReader(inputStream) );
			StringBuffer result = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				result.append( line );
			}
			return result.toString();
		} finally {
			if ( rd != null) {
				rd.close();
			}
		}
	}

	private String getCommandUrl(String event) {
		
		if ( nodoConnection.getCurrentHashKey() == null ) {
			// TODO throw exception
		}
		
		String commandUrl = MessageFormat.format(COMMAND_URL_FMT, nodoConnection.getNodoIP(),
				Integer.toString( nodoConnection.getNodoPort() ), event, nodoConnection.getCurrentHashKey() );
		
		commandUrl = StringUtils.replace(commandUrl, " ", "%20");
		logger.debug( commandUrl );
		return commandUrl;
	}
	}
