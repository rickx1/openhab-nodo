// $ANTLR 3.5 /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g 2014-12-28 07:40:08

package org.openhab.binding.nodo.internal.antlr.output;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.MessageFormat;

import org.openhab.binding.nodo.internal.model.events.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class NodoEventParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "BOOT", "COMMA", "FLOAT", "HEX", 
		"HexDigit", "ID", "INT", "KAKU", "NEW_KAKU", "OFF_STATE", "ON_STATE", 
		"USER_EVENT", "VARIABLE", "WS"
	};
	public static final int EOF=-1;
	public static final int BOOT=4;
	public static final int COMMA=5;
	public static final int FLOAT=6;
	public static final int HEX=7;
	public static final int HexDigit=8;
	public static final int ID=9;
	public static final int INT=10;
	public static final int KAKU=11;
	public static final int NEW_KAKU=12;
	public static final int OFF_STATE=13;
	public static final int ON_STATE=14;
	public static final int USER_EVENT=15;
	public static final int VARIABLE=16;
	public static final int WS=17;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public NodoEventParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public NodoEventParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return NodoEventParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g"; }


		private static final Logger logger = LoggerFactory.getLogger(NodoEventParser.class);
		private static final String LOG_ERROR_PARSING_EVENT = "Error parsing event ''{0}''";

		public static NodoEvent parse(String eventString, int unit)
		{               
			NodoEventLexer lexer = new NodoEventLexer( new ANTLRStringStream( eventString ) );
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			NodoEventParser parser = new NodoEventParser(tokens);
			
			try {
				NodoEvent nodoEvent = parser.expression();
				if ( nodoEvent != null) {
					nodoEvent.setUnit(unit);
				}
				return nodoEvent;
			} catch (RecognitionException e) {
				// Ignore unknown events, but do log them! 
				logger.error(MessageFormat.format(LOG_ERROR_PARSING_EVENT, eventString), e);
				return null;
			}
		}



	// $ANTLR start "expression"
	// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:49:1: expression returns [NodoEvent value] : (b= bootExpression |k= kakuExpression |n= newKakuExpression |v= variableExpression |u= userEventExpression );
	public final NodoEvent expression() throws RecognitionException  {
		NodoEvent value = null;


		NodoEvent b =null;
		NodoEvent k =null;
		NodoEvent n =null;
		NodoEvent v =null;
		NodoEvent u =null;

		try {
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:50:2: (b= bootExpression |k= kakuExpression |n= newKakuExpression |v= variableExpression |u= userEventExpression )
			int alt1=5;
			switch ( input.LA(1) ) {
			case BOOT:
				{
				alt1=1;
				}
				break;
			case KAKU:
				{
				alt1=2;
				}
				break;
			case NEW_KAKU:
				{
				alt1=3;
				}
				break;
			case VARIABLE:
				{
				alt1=4;
				}
				break;
			case USER_EVENT:
				{
				alt1=5;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}
			switch (alt1) {
				case 1 :
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:50:4: b= bootExpression
					{
					pushFollow(FOLLOW_bootExpression_in_expression43);
					b=bootExpression();
					state._fsp--;


							value = b;
						
					}
					break;
				case 2 :
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:54:4: k= kakuExpression
					{
					pushFollow(FOLLOW_kakuExpression_in_expression53);
					k=kakuExpression();
					state._fsp--;


							value = k;
						
					}
					break;
				case 3 :
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:58:4: n= newKakuExpression
					{
					pushFollow(FOLLOW_newKakuExpression_in_expression63);
					n=newKakuExpression();
					state._fsp--;


							value = n; 
						
					}
					break;
				case 4 :
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:62:4: v= variableExpression
					{
					pushFollow(FOLLOW_variableExpression_in_expression73);
					v=variableExpression();
					state._fsp--;


							value = v;
						
					}
					break;
				case 5 :
					// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:66:4: u= userEventExpression
					{
					pushFollow(FOLLOW_userEventExpression_in_expression84);
					u=userEventExpression();
					state._fsp--;


							value = u;
						
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return value;
	}
	// $ANTLR end "expression"



	// $ANTLR start "bootExpression"
	// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:72:1: bootExpression returns [NodoEvent value] : BOOT i= INT ;
	public final NodoEvent bootExpression() throws RecognitionException  {
		NodoEvent value = null;


		Token i=null;

		try {
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:73:2: ( BOOT i= INT )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:73:4: BOOT i= INT
			{
			match(input,BOOT,FOLLOW_BOOT_in_bootExpression104); 
			i=(Token)match(input,INT,FOLLOW_INT_in_bootExpression108); 

					return new BootEvent( (i!=null?i.getText():null) );
				
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return value;
	}
	// $ANTLR end "bootExpression"



	// $ANTLR start "kakuExpression"
	// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:79:1: kakuExpression returns [NodoEvent value] : KAKU i= ( ID | HEX ) COMMA s= ( ON_STATE | OFF_STATE ) ;
	public final NodoEvent kakuExpression() throws RecognitionException  {
		NodoEvent value = null;


		Token i=null;
		Token s=null;

		try {
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:80:2: ( KAKU i= ( ID | HEX ) COMMA s= ( ON_STATE | OFF_STATE ) )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:80:4: KAKU i= ( ID | HEX ) COMMA s= ( ON_STATE | OFF_STATE )
			{
			match(input,KAKU,FOLLOW_KAKU_in_kakuExpression128); 
			i=input.LT(1);
			if ( input.LA(1)==HEX||input.LA(1)==ID ) {
				input.consume();
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			match(input,COMMA,FOLLOW_COMMA_in_kakuExpression141); 
			s=input.LT(1);
			if ( (input.LA(1) >= OFF_STATE && input.LA(1) <= ON_STATE) ) {
				input.consume();
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}

					return new KaKuEvent( (i!=null?i.getText():null), (s!=null?s.getText():null) );
				
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return value;
	}
	// $ANTLR end "kakuExpression"



	// $ANTLR start "newKakuExpression"
	// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:86:1: newKakuExpression returns [NodoEvent value] : NEW_KAKU i= ( HEX | INT ) COMMA s= ( ON_STATE | OFF_STATE | INT ) ;
	public final NodoEvent newKakuExpression() throws RecognitionException  {
		NodoEvent value = null;


		Token i=null;
		Token s=null;

		try {
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:87:2: ( NEW_KAKU i= ( HEX | INT ) COMMA s= ( ON_STATE | OFF_STATE | INT ) )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:87:4: NEW_KAKU i= ( HEX | INT ) COMMA s= ( ON_STATE | OFF_STATE | INT )
			{
			match(input,NEW_KAKU,FOLLOW_NEW_KAKU_in_newKakuExpression171); 
			i=input.LT(1);
			if ( input.LA(1)==HEX||input.LA(1)==INT ) {
				input.consume();
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			match(input,COMMA,FOLLOW_COMMA_in_newKakuExpression184); 
			s=input.LT(1);
			if ( input.LA(1)==INT||(input.LA(1) >= OFF_STATE && input.LA(1) <= ON_STATE) ) {
				input.consume();
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}

					return new NewKaKuEvent( (i!=null?i.getText():null), (s!=null?s.getText():null) );
				
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return value;
	}
	// $ANTLR end "newKakuExpression"



	// $ANTLR start "variableExpression"
	// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:92:1: variableExpression returns [NodoEvent value] : VARIABLE i= INT COMMA v= ( FLOAT | INT ) ;
	public final NodoEvent variableExpression() throws RecognitionException  {
		NodoEvent value = null;


		Token i=null;
		Token v=null;

		try {
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:93:2: ( VARIABLE i= INT COMMA v= ( FLOAT | INT ) )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:93:4: VARIABLE i= INT COMMA v= ( FLOAT | INT )
			{
			match(input,VARIABLE,FOLLOW_VARIABLE_in_variableExpression218); 
			i=(Token)match(input,INT,FOLLOW_INT_in_variableExpression222); 
			match(input,COMMA,FOLLOW_COMMA_in_variableExpression224); 
			v=input.LT(1);
			if ( input.LA(1)==FLOAT||input.LA(1)==INT ) {
				input.consume();
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}

					return new VariableEvent( (i!=null?i.getText():null), Double.parseDouble((v!=null?v.getText():null)) );
				
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return value;
	}
	// $ANTLR end "variableExpression"



	// $ANTLR start "userEventExpression"
	// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:99:1: userEventExpression returns [NodoEvent value] : USER_EVENT id1= INT COMMA id2= INT ;
	public final NodoEvent userEventExpression() throws RecognitionException  {
		NodoEvent value = null;


		Token id1=null;
		Token id2=null;

		try {
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:100:2: ( USER_EVENT id1= INT COMMA id2= INT )
			// /Users/ricardobelfor/git/openhab-nodo/org.openhab.binding.nodo/src/main/java/org/openhab/binding/nodo/internal/antlr/NodoEvent.g:100:4: USER_EVENT id1= INT COMMA id2= INT
			{
			match(input,USER_EVENT,FOLLOW_USER_EVENT_in_userEventExpression257); 
			id1=(Token)match(input,INT,FOLLOW_INT_in_userEventExpression261); 
			match(input,COMMA,FOLLOW_COMMA_in_userEventExpression263); 
			id2=(Token)match(input,INT,FOLLOW_INT_in_userEventExpression267); 

					return new UserEventEvent( (id1!=null?id1.getText():null) + ","  + (id2!=null?id2.getText():null) );
				
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return value;
	}
	// $ANTLR end "userEventExpression"

	// Delegated rules



	public static final BitSet FOLLOW_bootExpression_in_expression43 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_kakuExpression_in_expression53 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_newKakuExpression_in_expression63 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variableExpression_in_expression73 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_userEventExpression_in_expression84 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BOOT_in_bootExpression104 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_INT_in_bootExpression108 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_KAKU_in_kakuExpression128 = new BitSet(new long[]{0x0000000000000280L});
	public static final BitSet FOLLOW_set_in_kakuExpression132 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_COMMA_in_kakuExpression141 = new BitSet(new long[]{0x0000000000006000L});
	public static final BitSet FOLLOW_set_in_kakuExpression145 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEW_KAKU_in_newKakuExpression171 = new BitSet(new long[]{0x0000000000000480L});
	public static final BitSet FOLLOW_set_in_newKakuExpression175 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_COMMA_in_newKakuExpression184 = new BitSet(new long[]{0x0000000000006400L});
	public static final BitSet FOLLOW_set_in_newKakuExpression188 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VARIABLE_in_variableExpression218 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_INT_in_variableExpression222 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_COMMA_in_variableExpression224 = new BitSet(new long[]{0x0000000000000440L});
	public static final BitSet FOLLOW_set_in_variableExpression228 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_USER_EVENT_in_userEventExpression257 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_INT_in_userEventExpression261 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_COMMA_in_userEventExpression263 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_INT_in_userEventExpression267 = new BitSet(new long[]{0x0000000000000002L});
}
