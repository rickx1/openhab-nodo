/**
 * 
 */
package org.openhab.binding.nodo.internal.model;

/**
 * @author Ricardo
 *
 */
public class NodoVariable extends NodoResource {

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
