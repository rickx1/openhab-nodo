/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */

package org.openhab.binding.nodo.internal.model.events;

import org.openhab.core.library.types.OnOffType;
import org.openhab.core.types.State;

/**
 * This class represents a 'Klik-aan-Klik-uit' event sent by the Nodo.

 * @author Ricardo Belfor
 *
 */
public class KaKuEvent extends NodoEvent {

	private static final String KAKU_ON = "On";
	
	private final String value;

	public KaKuEvent(String id, String value) {
		super(id);
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public State getState() {
		return value.equalsIgnoreCase(KAKU_ON) ? OnOffType.ON : OnOffType.OFF;
	}

	@Override
	public String toString() {
		return "KaKuEvent [getValue()=" + getValue() + ", getState()="
				+ getState() + ", getId()=" + getId() + "]";
	}
}
