/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.nodo.internal;

import java.util.ArrayList;
import java.util.List;

import org.openhab.binding.nodo.NodoBindingProvider;
import org.openhab.binding.nodo.internal.model.NodoResourceType;
import org.openhab.core.binding.BindingConfig;
import org.openhab.core.items.Item;
import org.openhab.model.item.binding.AbstractGenericBindingProvider;
import org.openhab.model.item.binding.BindingConfigParseException;


/**
 * <p>This class can parse information from the generic binding format and 
 * provides Nodo binding information from it. It registers as a 
 * {@link NodoBindingProvider} service as well.</p></p>
 * 
 * <p>Here are some examples for valid binding configuration strings:
 * <ul>
 * 	<li><code>{ nodo="type=KaKuSwitch; id=L1" }</code></li>
 * </ul>
 * 
 * @author Ricardo Belfor
 * @since 1.5.0
 */
public class NodoGenericBindingProvider extends AbstractGenericBindingProvider implements NodoBindingProvider {

	/**
	 * {@inheritDoc}
	 */
	public String getBindingType() {
		return "nodo";
	}

	/**
	 * @{inheritDoc}
	 */
	@Override
	public void validateItemType(Item item, String bindingConfig) throws BindingConfigParseException {
//		if ( !(item instanceof SwitchItem) && !(item instanceof) ) {
//			throw new BindingConfigParseException("item '" + item.getName()
//			+ "' is of type '" + item.getClass().getSimpleName()
//			+ "', only SwitchItems are allowed - please check your *.items configuration");
//		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void processBindingConfiguration(String context, Item item, String bindingConfig) throws BindingConfigParseException {

		super.processBindingConfiguration(context, item, bindingConfig);

		ConfigParser parser = new ConfigParser(bindingConfig);
		NodoConfig nodoConfig = parser.parse();
		
		NodoBindingConfig config;

		switch (nodoConfig.getType() ) {
		case NEW_KAKU_SWITCH:
			config = new NodoBindingConfig(NodoResourceType.NEW_KAKU_SWITCH, nodoConfig, item );
			break;
		case KAKU_SWITCH:
			config = new NodoBindingConfig(NodoResourceType.KAKU_SWITCH, nodoConfig, item );
			break;
		case VARIABLE:
			config = new NodoBindingConfig(NodoResourceType.VARIABLE, nodoConfig, item );
			break;
		case ACTIVITY:
			config = new NodoBindingConfig(NodoResourceType.ACTIVITY, nodoConfig, item );
			break;
		default:
			throw new BindingConfigParseException(bindingConfig);
		}

		addBindingConfig(item, config);		
	}

	@Override
	public Class<? extends Item> getItemType(String itemName) {
		NodoBindingConfig config = (NodoBindingConfig) bindingConfigs.get(itemName);
		if (config != null) {
			return config.getItemType();
		}
		return null;
	}

	@Override
	public NodoBindingConfig getNodoBindingConfigByName(String itemName) {
		return (NodoBindingConfig) bindingConfigs.get(itemName);
	}

	@Override
	public List<NodoBindingConfig> getNodoBindingConfigById(String id, int unit) {
		ArrayList<NodoBindingConfig> configs = new ArrayList<>();
		
		if (id != null) {
			for ( BindingConfig config : bindingConfigs.values() ) {
				NodoBindingConfig nodoBindingConfig = (NodoBindingConfig)config;
				if ( id.equals(nodoBindingConfig.getResourceId() ) && 
						nodoBindingConfig.getUnit() == unit) {
					configs.add(nodoBindingConfig);
				}
			}
		}

		return configs;
	}
}
