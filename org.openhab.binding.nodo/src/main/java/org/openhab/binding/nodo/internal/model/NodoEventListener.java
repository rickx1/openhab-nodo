/**
 * 
 */
package org.openhab.binding.nodo.internal.model;

import org.openhab.binding.nodo.internal.model.events.NodoEvent;

/**
 * @author Ricardo Belfor
 *
 */
public interface NodoEventListener {

	void eventReceived(NodoEvent event);
}
