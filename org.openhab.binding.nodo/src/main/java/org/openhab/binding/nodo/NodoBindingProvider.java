/**
 * Copyright (c) 2010-2014, openHAB.org and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.nodo;

import java.util.List;

import org.openhab.binding.nodo.internal.NodoBindingConfig;
import org.openhab.core.binding.BindingProvider;
import org.openhab.core.items.Item;

/**
 * This interface is implemented by classes that can provide mapping information
 * between openHAB items and Nodo items.
 * 
 * Implementing classes should register themselves as a service in order to be 
 * taken into account.
  * 
 * @author Ricardo Belfor
 * @since 1.5.0
 */
public interface NodoBindingProvider extends BindingProvider {
	
	/**
	 * Returns the Type of the Item identified by {@code itemName}
	 * 
	 * @param itemName the name of the item to find the type for
	 * @return the type of the Item identified by {@code itemName}
	 */
	Class<? extends Item> getItemType(String itemName);
	
	/**
	 * Returns the binding config details associated with an <code>itemName</code>
	 * or <code>null</code> if it could not be found.
	 * 
	 */
	NodoBindingConfig getNodoBindingConfigByName(String itemName);
	
	
	/**
	 * Returns the binding config details associated with an <code>id</code>
	 * or an empty collection if it could not be found.
	 * @param unit the nodo unit that generated the event
	 * 
	 */
	List<NodoBindingConfig> getNodoBindingConfigById(String id, int unit);
	
}